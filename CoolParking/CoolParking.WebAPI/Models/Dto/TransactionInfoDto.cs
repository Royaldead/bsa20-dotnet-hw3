﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;

namespace CoolParking.WebAPI.Models.Dto
{
    public class TransactionInfoDto
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
}
