﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.Models.Dto
{
    public class VehicleDto
    {
        [Required(ErrorMessage = "Property 'id' is required")]
        [JsonProperty("id")]
        public string Id { get; set; }
        [Required(ErrorMessage = "Property 'vehicleType' is required")]
        [JsonProperty("vehicleType")]
        [Range(0, 3, ErrorMessage = "Property 'vehicleType' must be is range [0...3]")]
        public int? VehicleType { get; set; }
        [Required(ErrorMessage = "Property 'balance' is required")]
        [JsonProperty("balance")]
        public decimal? Balance { get; set; }
    }
}
