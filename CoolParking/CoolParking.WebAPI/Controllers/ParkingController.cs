﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {

        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parkingService.GetBalance());
        }

        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }

        [HttpGet("profit")]
        public ActionResult<decimal> GetProfitSinceTheLAstLog()
        {
            return Ok(_parkingService.GetLastParkingTransactions().ToList().Sum(tr => tr.Sum));
        }
    }
}
