﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.IO;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {

        private readonly IParkingService _parkingService;
        private readonly IMapper _autoMapper;

        public VehiclesController(IParkingService parkingService, IMapper autoMapper)
        {
            _parkingService = parkingService;
            _autoMapper = autoMapper;
        }

        [HttpGet]
        public ActionResult GetVehicles()
        {
            var vehicles = _parkingService.GetVehicles();
            return Ok(vehicles);
        }

        [HttpGet("{id}")]
        public ActionResult GetVehicleById(string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                return BadRequest("Wrong vehicle ID");
            }
            var vehicles = _parkingService.GetVehicles();
            var vehicle = vehicles.ToList().Find(v => v.Id == id);
            if (vehicle == null)
            {
                return NotFound("Vehicle with given ID is not in the parking");
            }
            return Ok(vehicle);
        }

        [HttpPost]
        public ActionResult AddVehicle([FromBody]VehicleDto vehicleDto)
        {
            try
            {
                Vehicle vehicle = _autoMapper.Map<VehicleDto, Vehicle>(vehicleDto);
                _parkingService.AddVehicle(vehicle);
                return StatusCode(201, vehicle);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult RemoveVehicle(string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                return BadRequest("Wrong vehicle ID");
            }
            try
            {
                _parkingService.RemoveVehicle(id);
                return StatusCode(204);
            }
            catch (ArgumentException argEx)
            {
                return NotFound(argEx.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
