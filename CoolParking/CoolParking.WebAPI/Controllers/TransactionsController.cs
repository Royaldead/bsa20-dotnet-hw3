﻿using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Models.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {

        private readonly IParkingService _parkingService;
        private readonly IMapper _autoMapper;

        public TransactionsController(IParkingService parkingService, IMapper autoMapper)
        {
            _parkingService = parkingService;
            _autoMapper = autoMapper;
        }

        [HttpGet("last")]
        public ActionResult<List<TransactionInfo>> GetTheLastTransactions()
        {
            List<TransactionInfo> transactions = _parkingService.GetLastParkingTransactions().ToList();
            var transactionsDto = _autoMapper.Map<List<TransactionInfo>, List<TransactionInfoDto>>(transactions);
            return Ok(transactionsDto);
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                string allTransactions = _parkingService.ReadFromLog();
                return Ok(allTransactions);
            }
            catch (InvalidOperationException invOpexception)
            {
                return NotFound(invOpexception.Message);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception.Message);
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody]TopUp topUp)
        {
            if (!Regex.IsMatch(topUp.Id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
            {
                return BadRequest("Wrong vehicle ID");
            }
            if (topUp.Sum <= 0)
            {
                return BadRequest("Top up sum must be greater than 0");
            }
            try
            {
                _parkingService.TopUpVehicle(topUp.Id, topUp.Sum);
                Vehicle vehicle = _parkingService.GetVehicles().ToList().Find(v => v.Id == topUp.Id);
                return Ok(vehicle);
            }
            catch (ArgumentException argException)
            {
                return NotFound(argException.Message);
            }
        }
    }
}
