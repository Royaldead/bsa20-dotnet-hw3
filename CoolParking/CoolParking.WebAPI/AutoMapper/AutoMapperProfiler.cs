﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models.Dto;

namespace CoolParking.WebAPI.AutoMapper
{
    public class AutoMapperProfiler : Profile
    {
        public AutoMapperProfiler()
        {
            CreateMap<VehicleDto, Vehicle>();
            CreateMap<TransactionInfo, TransactionInfoDto>()
                .ForMember(dest => dest.TransactionDate, opt => opt.MapFrom(src => src.Time));
        }
    }
}
