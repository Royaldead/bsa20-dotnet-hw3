﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.WebUI.Models
{
    class Transaction
    {
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }
        public DateTime TransactionDate { get; set; }

        public override string ToString()
        {
            return $"Transaction time: {TransactionDate.ToLocalTime()}; vehicle ID: {VehicleId}; withdrawn sum: {Sum}";
        }
    }
}
