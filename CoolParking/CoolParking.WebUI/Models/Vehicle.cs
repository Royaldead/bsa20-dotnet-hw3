﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.WebUI.Models
{
    class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }
        public Vehicle(string id, int vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public string GetVehicleType()
        {
            switch (VehicleType)
            {
                case 0:
                    return "Passenger Car";
                case 1:
                    return "Truck";
                case 2:
                    return "Bus";
                case 3:
                    return "Motorcycle";
                default:
                    return String.Empty;
            }
        }

    }
}
