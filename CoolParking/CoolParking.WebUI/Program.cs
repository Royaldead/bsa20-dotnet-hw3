﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Net.Http;
using System.Reflection;

namespace CoolParking.CLI
{ 
    class Program
    {
        public static IConfigurationRoot configuration;
        static void Main(string[] args)
        {
            configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
            .AddJsonFile("appsettings.json", false)
            .Build();

            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(configuration.GetSection("baseUrl").Value);

            Console.WriteLine("***Welcome to our parking!***");
            while (true)
            {
                Console.WriteLine("\nChoose your action (type a digit and press 'Enter'):");
                Console.WriteLine("1. Show parking balance");
                Console.WriteLine("2. Show profit since last log");
                Console.WriteLine("3. Show free parking spaces");
                Console.WriteLine("4. Show transactions since the last log");
                Console.WriteLine("5. Show all-time transactions");
                Console.WriteLine("6. Show vehicles in the parking");
                Console.WriteLine("7. Put a vehicle in the parking");
                Console.WriteLine("8. Remove a vehicle from the parking");
                Console.WriteLine("9. Top up your balance");
                Console.WriteLine("0. Close\n");

                string stringAnswer = Console.ReadLine();

                bool parseAnswerSuccess = Int32.TryParse(stringAnswer, out int intAnswer);
                if (!parseAnswerSuccess)
                {
                    Console.WriteLine("Oops! You typed wrong value. Please, try again");
                    continue;
                }
                else if (intAnswer == 0)
                {
                    Console.WriteLine("\nAre you sure you want to exit? Type a digit and press 'Enter':\n");
                    Console.WriteLine("1. Yes");
                    Console.WriteLine("2. No (Default)\n");
                    string continueConfirmation = Console.ReadLine();
                    bool parseContinueAnswerSuccess = Int32.TryParse(continueConfirmation, out int continueAnswer);
                    if (continueAnswer == 1)
                    {
                        Console.WriteLine("\nThank you for using our service. See you next time. Good luck!");
                        httpClient.Dispose();
                        break;
                    }
                }
                else
                {
                    WebUIMethods.ChooseAndExecuteMethod(httpClient, intAnswer);

                    Console.WriteLine("\nPress any key to continue\n");
                    Console.ReadKey();
                    Console.WriteLine();
                }
            }
        }
    }
}
