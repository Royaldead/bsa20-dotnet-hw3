﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.WebUI.Models;
using Newtonsoft.Json.Serialization;

namespace CoolParking.CLI
{
    static class WebUIMethods
    {
        public static void ChooseAndExecuteMethod(HttpClient httpClient, int intAnswer)
        {
            switch (intAnswer)
            {
                case 1:
                    ShowParkingBalance(httpClient);
                    break;
                case 2:
                    ShowProfitSinceLastLog(httpClient);
                    break;
                case 3:
                    ShowFreeParkingSpaces(httpClient);
                    break;
                case 4:
                    ShowTransactionsSinceTheLastLog(httpClient);
                    break;
                case 5:
                    ShowAllTimeTransactions(httpClient);
                    break;
                case 6:
                    ShowVehiclesInTheParking(httpClient);
                    break;
                case 7:
                    PutVehicleInTheParking(httpClient);
                    break;
                case 8:
                    RemoveVehicleFromTheParking(httpClient);
                    break;
                case 9:
                    TopUpYourBalance(httpClient);
                    break;
                default:
                    break;
            }
        }

        private static void TopUpYourBalance(HttpClient httpClient)
        {
            string vehicleId;
            decimal balance;

            vehicleId = GetVehicleId();
            if (vehicleId == null)
            {
                return;
            }
            balance = GetBalance();
            if (balance == decimal.MinValue)
            {
                return;
            }

            var topUpdata = new { id = vehicleId, sum = balance };
            string json = JsonConvert.SerializeObject(topUpdata);
            StringContent transferData = new StringContent(json, Encoding.UTF8, "application/json");
            string url = "transactions/topUpVehicle";
            var response = httpClient.PutAsync(url, transferData);
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                Console.WriteLine($"\nYour balance has been topped up by {balance}");
            }
            else
            {
                Console.WriteLine($"\nError: {response.Result.Content.ReadAsStringAsync().Result}");
            }

        }

        private static void RemoveVehicleFromTheParking(HttpClient httpClient)
        {
            string vehicleId;
            vehicleId = GetVehicleId();
            if (vehicleId == null)
            {
                return;
            }

            string removeVehicleUrl = $"vehicles/{vehicleId}";
            var removeVehicleResponse = httpClient.DeleteAsync(removeVehicleUrl);
            removeVehicleResponse.Wait();

            if (removeVehicleResponse.Result.IsSuccessStatusCode)
            {
                Console.WriteLine("\nVehicle removed");
            }
            else
            {
                Console.WriteLine($"\nError: {removeVehicleResponse.Result.Content.ReadAsStringAsync().Result}");
            }

        }

        private static void PutVehicleInTheParking(HttpClient httpClient)
        {
            string vehicleId;
            int vehicleType;
            decimal balance;

            vehicleId = GetVehicleId();
            if (vehicleId == null)
            {
                return;
            }

            int enumValue = GetVehicleType();
            if (enumValue == Int32.MinValue)
            {
                return;
            }
            else
            {
                vehicleType = enumValue;
            }

            balance = GetBalance();
            if (balance == decimal.MinValue)
            {
                return;
            }

            Vehicle vehicle = new Vehicle(vehicleId, vehicleType, balance);

            var topUpdata = new { id = vehicleId, vehicleType, balance };
            string json = JsonConvert.SerializeObject(vehicle);
            
            StringContent transferData = new StringContent(json, Encoding.UTF8, "application/json");
            string url = "vehicles";
            var response = httpClient.PostAsync(url, transferData);
            response.Wait();
            if (response.Result.IsSuccessStatusCode)
            {
                Console.WriteLine($"\nVehicle added");
            }
            else
            {
                Console.WriteLine($"\nError: {response.Result.Content.ReadAsStringAsync().Result}");
            }

        }

        private static void ShowVehiclesInTheParking(HttpClient httpClient)
        {
            string url = "vehicles";
            var response = httpClient.GetAsync(url);
            response.Wait();
            if (!response.Result.IsSuccessStatusCode)
            {
                Console.WriteLine($"\nError: {response.Result.Content.ReadAsStringAsync().Result}");
                return;
            }
            var vehiclesJson = response.Result.Content.ReadAsStringAsync().Result;
            var vehicles = JsonConvert.DeserializeObject<Vehicle[]>(vehiclesJson);
            if (vehicles.ToList().Count == 0)
            {
                Console.WriteLine("\nThere are no vehicles in the parking now");
                return;
            }
            Console.WriteLine("\nThere are vehicles with such ID's in the parking now:\n");
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"ID: {vehicle.Id}, type: {vehicle.GetVehicleType()}");
            }
        }

        private static void ShowAllTimeTransactions(HttpClient httpClient)
        {
                string allTransactionsUrl = "transactions/all";
                var allTransactionsResponse = httpClient.GetAsync(allTransactionsUrl);
                allTransactionsResponse.Wait();
                if (!allTransactionsResponse.Result.IsSuccessStatusCode)
                {
                    Console.WriteLine($"\nError: {allTransactionsResponse.Result.Content.ReadAsStringAsync().Result}");
                    return;
                }
                string logFile = allTransactionsResponse.Result.Content.ReadAsStringAsync().Result;
                if (String.IsNullOrEmpty(logFile))
                {
                    Console.WriteLine("\nFile Transactions.log is empty");
                    return;
                }
                Console.WriteLine("\n*** The content of Transactions.log ***");
                Console.WriteLine(logFile);
        }

        private static void ShowTransactionsSinceTheLastLog(HttpClient httpClient)
        {
            string lastTransactionsUrl = "transactions/last";
            var lastTransactionsResponse = httpClient.GetAsync(lastTransactionsUrl);
            lastTransactionsResponse.Wait();
            var lastTransactionsJson = lastTransactionsResponse.Result.Content.ReadAsStringAsync().Result;
            Transaction[] lastTransactions = JsonConvert.DeserializeObject<Transaction[]>(lastTransactionsJson);
            if (lastTransactions.ToList().Count == 0)
            {
                Console.WriteLine("\nThere are no transactions since the last log");
                return;
            }
            Console.WriteLine($"\n*** Transactions since the last log: ***");
            foreach (var transaction in lastTransactions)
            {
                Console.WriteLine(transaction.ToString());
            }
        }

        private static void ShowFreeParkingSpaces(HttpClient httpClient)
        {
            string capacity = String.Empty;
            string freePlaces = String.Empty;
            Task[] tasks = new Task[2]
                {
                    new Task(() => 
                    {
                        string capacityUrl = "parking/capacity";
                        var capacityResponse = httpClient.GetAsync(capacityUrl);
                        capacity = capacityResponse.Result.Content.ReadAsStringAsync().Result;
                    }),
                    new Task(() =>
                    {
                        string freePlacesUrl = "parking/freePlaces";
                        var freePlacesResponse = httpClient.GetAsync(freePlacesUrl);
                        freePlaces = freePlacesResponse.Result.Content.ReadAsStringAsync().Result;
                    })
                };
            foreach (var task in tasks)
                task.Start();

            Task.WaitAll(tasks);

            Console.WriteLine($"\nThere are {freePlaces} free spaces of {capacity} in the parking");
        }

        private static void ShowProfitSinceLastLog(HttpClient httpClient)
        {
            string url = "parking/profit";
            var response = httpClient.GetAsync(url);
            response.Wait();
            var profit = response.Result.Content.ReadAsStringAsync().Result;
            Console.WriteLine($"\nParking profit since last log: {profit}");
        }

        private static void ShowParkingBalance(HttpClient httpClient)
        {
            string url = "parking/balance";
            var response = httpClient.GetAsync(url);
            response.Wait();
            var balance = response.Result.Content.ReadAsStringAsync().Result;
            Console.WriteLine($"\nCurrent parking balance: {balance}");
        }

        private static string GetVehicleId()
        {
            while (true)
            {
                Console.WriteLine("\nType vehicle ID (plate number) in format ХХ-YYYY-XX,\nwhere X - letter and Y - digit or type 'q' and press 'Enter' to exit:\n");
                string stringAnswer = Console.ReadLine().ToUpper();

                if (stringAnswer == "Q")
                {
                    Console.WriteLine("\nExiting...");
                    return null;
                }
                else if (!Regex.IsMatch(stringAnswer, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
                {
                    Console.WriteLine("\nWrong vehicle ID has been typed. Please, try again.");
                    continue;
                }
                else
                {
                    return stringAnswer;
                }
            }
        }

        private static int GetVehicleType()
        {
            while (true)
            {
                Console.WriteLine("\nChoose your vehicle type (type a digit and press 'Enter') or type 'q' and press 'Enter' to exit");
                Console.WriteLine("0. Passenger Car");
                Console.WriteLine("1. Truck");
                Console.WriteLine("2. Bus");
                Console.WriteLine("3. Motorcycle\n");
                string stringAnswer = Console.ReadLine().ToUpper();

                if (stringAnswer == "Q")
                {
                    Console.WriteLine("\nExiting...");
                    return Int32.MinValue;
                }

                bool parseAnswerSuccess = Int32.TryParse(stringAnswer, out int intAnswer);

                if (!parseAnswerSuccess || intAnswer < 0 || intAnswer > 3)
                {
                    Console.WriteLine("\nWrong vehicle type has been typed. Please, try again.");
                    continue;
                }
                else
                {
                    return intAnswer;
                }
            }
        }

        private static decimal GetBalance()
        {
            while (true)
            {
                Console.WriteLine("\nType a balance in format X.XX,\nwhere X - digit (must be greater than 0) or type 'q' and press 'Enter' to exit\n");
                string stringAnswer = Console.ReadLine().ToUpper();

                if (stringAnswer == "Q")
                {
                    Console.WriteLine("\nExiting...");
                    return decimal.MinValue;
                }

                bool parseAnswerSuccess = Decimal.TryParse(stringAnswer, out decimal decimalAnswer);

                int numberOfDecimalPlaces = parseAnswerSuccess ? BitConverter.GetBytes(decimal.GetBits(decimalAnswer)[3])[2] : Int32.MaxValue;

                if (!parseAnswerSuccess || decimalAnswer <= 0 || numberOfDecimalPlaces > 2)
                {
                    Console.WriteLine("\nWrong balance has been typed. Please, try again.");
                    continue;
                }
                else
                {
                    return decimalAnswer;
                }
            }
        }
    }
}
