﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }
        public Timer Timer { get; set; }

        public event ElapsedEventHandler Elapsed;
        public TimerService()
        {
            Timer = new Timer();
        }

        public void Dispose()
        {
            Timer.Dispose();
        }

        public void Start()
        {
            Timer.Interval = Interval;
            Timer.Elapsed += Elapsed;
            Timer.AutoReset = true;
            Timer.Enabled = true;
        }

        public void Stop()
        {
            Timer.Stop();
        }

    }
}
