﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitalBalance { get; } = 0M;
        public static int Capacity { get; } = 10;
        public static int ChargeInterval { get; } = 5;
        public static int LogInterval { get; } = 60;
        public static decimal PassengerCarRate { get; } = 2M;
        public static decimal TruckRate { get; } = 5M;
        public static decimal BusRate { get; } = 3.5M;
        public static decimal MotorcycleRate { get; } = 1M;
        public static decimal PenaltyMultiplier { get; } = 2.5M;
    }
}
